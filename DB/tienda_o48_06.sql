create database tienda_o48_06;
use tienda_o48_06;
create table producto(
codigo_pro varchar(15) not null,
nombre_pro varchar(30) not null,
cantidad_pro int not null,
valor double not null,
constraint producto_pk primary key(codigo_pro));
create table proveedor(
codigo_prv varchar(15) not null,
nombre_prv varchar(30) not null,
constraint proveedor_pk primary key(codigo_prv));
create table cliente(
codigo_cli varchar(15) not null,
nombre_cle varchar(30) not null,
constraint cliente_pk primary key(codigo_cli));
create table factura_compra(
codigo_fac varchar(15) not null,
codigo_prv varchar(15) not null,
valor double not null,
constraint factura_compra_pk primary key(codigo_fac),
constraint factura_compra_codigo_prv_fk foreign key(codigo_prv) references proveedor(codigo_prv));
create table factura_venta(
codigo_fac varchar(15) not null,
codigo_cli varchar(15) not null,
valor double not null,
constraint factura_venta_pk primary key(codigo_fac),
constraint factura_venta_codigo_cli_fk foreign key(codigo_cli) references cliente(codigo_cli));
create table producto_venta(
codigo_pro varchar(15) not null,
codigo_fac varchar(15) not null,
cantidad int not null,
valor double not null,
constraint producto_venta_pk primary key(codigo_pro,codigo_fac),
constraint prodcuto_venta_codigo_pro_fk foreign key(codigo_pro) references producto(codigo_pro),
constraint producto_venta_codigo_fac_fk foreign key(codigo_fac) references factura_venta(codigo_fac));
create table producto_compra(
codigo_pro varchar(15) not null,
codigo_fac varchar(15) not null,
cantidad int not null,
valor double not null,
constraint producto_compra_pk primary key(codigo_pro,codigo_fac),
constraint prodcuto_compra_codigo_pro_fk foreign key(codigo_pro) references producto(codigo_pro),
constraint producto_compra_codigo_fac_fk foreign key(codigo_fac) references factura_compra(codigo_fac));



